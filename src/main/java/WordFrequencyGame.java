import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    private final static String ANYBLANKCHAR = "\\s+";
    private final static int INTI_COUNT = 1;

    public String getResult(String inputStr) {
        try {
            List<Input> inputList = splitWords(inputStr);
            Map<String, Long> map = countFrequencyWord(inputList);
            return sortDescendingWords(map);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private List<Input> splitWords(String words) {
        String[] arr = words.split(ANYBLANKCHAR);
        List<Input> inputList = new ArrayList<>();
        Arrays.stream(words.split(ANYBLANKCHAR))
                .forEach(shortStr -> inputList.add(new Input(shortStr, INTI_COUNT)));
        return inputList;
    }

    public String sortDescendingWords(Map<String, Long> inputList) {
        List<Input> list = new ArrayList<>();
        for (Map.Entry<String, Long> entry : inputList.entrySet()) {
            Input input = new Input(entry.getKey(), Math.toIntExact(entry.getValue()));
            list.add(input);
        }
        list.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
        return printResultInput(list);
    }

    private String printResultInput(List<Input> inputList) {
        StringJoiner joiner = new StringJoiner("\n");
        inputList.forEach(input -> joiner.add(input.getValue() + " " + input.getWordCount()));
        return joiner.toString();
    }

    private Map<String, Long> countFrequencyWord(List<Input> inputList) {
        Map<String, Long> map = inputList.stream()
                .collect(Collectors.groupingBy(Input::getValue,Collectors.counting()));
        return map;
    }
}
